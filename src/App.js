import React from 'react';
import logo from './logo.svg';
import './App.css';
import Sidebar from './uikitComponents/Sidebar'
import Guidelines from './pages/Guidelines'
import Color from './pages/Color'
import Typhography from './pages/Typhograpy'
import Listp from './pages/ListPage'
import Cardp from './pages/CardPage'
import Formp from './pages/FormsPage'
import {Route, Links} from 'react-router-dom'

const uiElements = [
  { name: 'guidelines', nameIcon:'description', labelElement: 'Guidelines', onPosition: true },
  { name: 'color', nameIcon:'palette', labelElement: 'Color', onPosition: false },
  { name: 'typography', nameIcon:'font_download', labelElement: 'Typography',  onPosition: false},
  { name: 'list', nameIcon:'list', labelElement: 'List View',  onPosition: false },
  { name: 'cards', nameIcon:'check_box_outline_blank', labelElement: 'Cards View',  onPosition: false },
  { name: 'forms', nameIcon:'date_range', labelElement: 'Forms',  onPosition: false },
]
const uiContent = [
  {title:'Guidelines'},
  {title:'Color'},
  {title:'Typhography'},
  {title:'List'},
  {title:'Cards'},
  {title:'List and Cards View'},
  {title:'Forms'},
]


function App() {
  return (
    <div className="App">
      <Sidebar items={uiElements}/>
      <Route exact path="/" component={Guidelines}></Route>
      <Route exact path="/guidelines" component={Guidelines}></Route>
      <Route exact path="/color" component={Color}></Route>
      <Route exact path="/typography" component={Typhography}></Route>
      <Route exact path="/list" component={Listp}></Route>
      <Route exact path="/cards" component={Cardp}></Route>
      <Route exact path="/forms" component={Formp}></Route>
    </div>
  );
}

export default App;
