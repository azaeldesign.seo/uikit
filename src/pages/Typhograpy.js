import React from 'react';
import Container from '@material-ui/core/Container';

const Typhography = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>Typography</h1>
        </div>
        <p>Se propone a Roboto como la tipografia principal es una de las tipografias más reconocidas por su legibilidad en sistemas web.
        </p>
        <div className="textwrapper">
            <h1>Titulo 1</h1>
            <h2>Titulo 2</h2>
            <h3>Titulo 3</h3>
            <p>Parrafo</p>
        </div>
        <div className="textwrapper blackw">
            <h1>Titulo 1</h1>
            <h2>Titulo 2</h2>
            <h3>Titulo 3</h3>
            <p>Parrafo</p>
        </div>
    </Container>
    )
}

export default Typhography;