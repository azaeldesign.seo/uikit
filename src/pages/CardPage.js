import React from 'react';
import Container from '@material-ui/core/Container';
import CardElement from '../uikitComponents/CardsElmt'

const Cardp = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>Cards</h1>
        </div>
        <p>Las cartas son elementos que pueden ser ultilizados para dividir y condensar información.
        </p>
        <CardElement 
        name="Fascinante película de acción" 
        headerTitle="John Wick 3" 
        plot="Tras asesinar a uno de los miembros de su gremio, Wick es expulsado y se convierte en el foco de atención de todos los sicarios de la organización" 
        source="https://i1.wp.com/wipy.tv/wp-content/uploads/2020/04/john-wick-es-parte-de-un-videojuego.jpg">
        </CardElement>
    </Container>
    )
}

export default Cardp;