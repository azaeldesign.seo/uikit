import React from 'react';
import Container from '@material-ui/core/Container';
import EasyList from '../uikitComponents/EasyList'

const peopleData = [
    { name: 'Elton John', phone:'818758943' },
    { name: 'Paul Mc', phone:'500758943' },
    { name: 'Ringo Star', phone:'500058943' },
    { name: 'Billie Eliot', phone:'500758943' },
    { name: 'Guillermo de Toro', phone:'500758943' },
  ]

const Listp = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>List</h1>
        </div>
        <p>Se establece la siguiente estilo de lista a ser utilizadas</p>

        <EasyList items={peopleData}/>
    </Container>
    )
}

export default Listp;