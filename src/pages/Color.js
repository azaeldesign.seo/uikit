import React from 'react';
import Container from '@material-ui/core/Container';
import ColorElmt from '../uikitComponents/ColorElmt'

const Color = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>Colores</h1>
        </div>
        <p>Se establecen 2 colores primarios sacados del sitio del estado. 4 colores informativos y 5 tonos de grises.
            Los cuales enlistamos a continuación:
        </p>
        <h2>Colores Principales de Marca</h2>
        <div className="row">
            <ColorElmt colorHex='#004c8c' colorName="Primary" colorNameClass="primary" ></ColorElmt>
            <ColorElmt colorHex='#5232C2' colorName="Secondary" colorNameClass="secondary"></ColorElmt>
        </div>
        <h2>Colores informativos</h2>
        <div className="row">
            <ColorElmt colorHex='#e61b0c' colorName="Rojo Error" colorNameClass="rederror"></ColorElmt>
            <ColorElmt colorHex='#f9bf12' colorName="Amarillo Alerta" colorNameClass="yellowalert"></ColorElmt>
            <ColorElmt colorHex='#0bde69' colorName="Verde Pass" colorNameClass="greenpass"></ColorElmt>
            <ColorElmt colorHex='#27aeff' colorName="Azul Exito" colorNameClass="bluesuccess"></ColorElmt>
        </div>
        <h2>Grises del sistema</h2>
        <div className="row">
            <ColorElmt colorHex='#353535' colorName="Gris 100" colorNameClass="gray-100"></ColorElmt>
            <ColorElmt colorHex='#737171' colorName="Gris 80" colorNameClass="gray-80"></ColorElmt>
            <ColorElmt colorHex='#929191' colorName="Gris 60" colorNameClass="gray-60"></ColorElmt>
            <ColorElmt colorHex='#f4f5f7' colorName="Gris 30" colorNameClass="gray-30"></ColorElmt>
            <ColorElmt colorHex='#fcfcfd' colorName="Gris 15" colorNameClass="gray-15"></ColorElmt>
        </div>
    </Container>
    )
}

export default Color;
  