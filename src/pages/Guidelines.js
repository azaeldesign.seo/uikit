import React from 'react';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

const Guidelines = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>Guidelines</h1>
        </div>
        
        <p>Se establecen los siguientes guidelines para los aplicativos de la subsecretaria del Estado de Nuevo León.
            En lo relacionado la usabilidad se utilizara de guía las <a href="https://www.nngroup.com/articles/ten-usability-heuristics/" target="_Blank">
            10 heurísticas de Nielsen.</a> Las cuales enlistamos a continuación
        </p>
        <ul>
            <li>Visibilidad del estado del sistema.</li>
            <li>Utilizar el lenguaje de los usuarios.</li>
            <li>Control y libertad para el usuario.</li>
            <li>Consistencia y estándares.</li>
            <li>Prevención de errores.</li>
            <li>Minimizar la carga de la memoria del usuario.</li>
            <li>Flexibilidad y eficiencia de uso.</li>
            <li>Los diálogos estéticos y diseño minimalista.</li>
            <li>Ayudar a los usuarios a reconocer, diagnosticar y recuperarse de los errores.
            Ayuda y documentación.</li>
        </ul>
        
        <h2>UI Kit</h2>
        <p>
            En este  UI Kit se establecen unas guías de los componentes que deben de ultilizarse, asi mismo todo el contenido se rige de una libreria principal que da pie, 
            pero no limita la creación de nuevos componentes como se vayan necesitando.
        </p>
        <h3>Botones</h3>
        <p>En este apartado se incluyen los botones disponibles a ultilizarse. El principal para los call to action</p>
        <div className="buttonContainer">
            <Button variant="contained" color="primary" >
                Enviar <Icon>send</Icon>
            </Button>
            <Button variant="outlined" color="primary" >
                Something <Icon>chevron_left</Icon>
            </Button>
            <Button variant="outlined" color="secondary" >
                Something <Icon>chevron_left</Icon>
            </Button>
        </div>
    </Container>
    )
}

export default Guidelines;
