import React from 'react';
import Container from '@material-ui/core/Container';
import FormEl from '../uikitComponents/FormEl'

const Formp = () => {
    return (
    <Container>
        <div className="header-app">
            <h1>Forms</h1>
        </div>
        <p>Los formularios son elementos muy importantes que se encargan de obtener infomarción de parte del usuario.</p>
    
        <FormEl 
        title="Subscríbete al Boletín" 
        inst="Mantente informado por medio de nuestro boletín"
        icon="send"
        callAction="Subscríbete">
        </FormEl>
    </Container>
    )
}

export default Formp;