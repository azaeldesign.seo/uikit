import React from 'react';
import { Icon, Button } from '@material-ui/core';

const CardElement =(props)=>{
    return(
        <section className="card">
            <header>
                <strong>{props.headerTitle}</strong>
                <Icon>favorite_border</Icon>
            </header>
            <figure>
                <img src={props.source}/>
            </figure>
            <div className="cs">
                <strong>{props.name} </strong>
                <p className='plot'>
                    {props.plot}
                </p>
                <div className="buttonWrapper">
                    <Button variant="contained" color="primary" >
                        Comprar
                    </Button>
                    <Button variant="outlined" color="primary" >
                        <Icon>shopping_cart</Icon> Rentar
                    </Button>
                </div>
            </div>
        </section>
    )
}
export default CardElement;