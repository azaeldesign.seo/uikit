import React from 'react';
import PropTypes from "prop-types";
import Icon from '@material-ui/core/Icon';

function Contact(props) {
  return (
    <div className="listElement">
      <span><Icon>insert_emoticon</Icon> {props.name}</span>
        <span><Icon>call</Icon> {props.phone}</span>
    </div>
  );
}

Contact.propTypes = {
  name: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired
};

const EasyList = ({ items }) => {
    return (<div className="easylist">
        {items.map(({name, phone }) => (
            <Contact key={name} name={name} phone={phone}></Contact> 
        ))}
    </div>)
}

export default EasyList;