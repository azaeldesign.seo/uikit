import React from 'react';

const ColorElmt = ({colorHex, colorName, colorNameClass} ) => {
    return (
    
    <div className="col-3 color-element">
        <div className={colorNameClass}>
            <div className='colorData'>
                <h4>{colorName}</h4>
                <p>{colorHex}</p>
            </div>
        </div>
    </div>
    );
}
export default ColorElmt; 