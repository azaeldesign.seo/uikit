import React from 'react';
import Icon from '@material-ui/core/Icon';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';

const FormEl = (props) => {
    return (
        <div class="simpleForm">
            <header>
                <strong>{props.title}</strong>
                <p>{props.inst}</p>
            </header>
            <form>
                <div className="formField">
                    <Input name="Nombre" placeholder="Nombre" fullWidth="true"></Input>
                </div>
                <div className="formField">
                    <Input name="Correo" placeholder="Correo" type='email' fullWidth="true"></Input>
                </div>
                <div className="formField sendButton">
                    <Button variant="contained" fullWidth={true} color="primary" >
                        {props.callAction} 
                        <Icon>{props.icon}</Icon>
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default FormEl;