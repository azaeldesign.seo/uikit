import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}
const Sidebar = ({ items }) => {
    return (
    <div className="sidebar">
        <List disablePadding dense>
        {items.map(({ labelElement, name, nameIcon, onPosition, ...rest }) => (
        <ListItemLink
        key={name} 
        button {...rest} 
        href={'/' + name}
        >
            <ListItemIcon>
                <Icon>{nameIcon}</Icon>
            </ListItemIcon>
          <ListItemText>{labelElement}</ListItemText>
        </ListItemLink>
      ))}
      </List>
    </div>
    ) 
}

export default Sidebar; 